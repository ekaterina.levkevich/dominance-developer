import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class MainPastebinPage {
    private WebDriver driver;
    public MainPastebinPage(WebDriver driver) {
        this.driver = driver;
        driver.get("https://pastebin.com");
    }

    private WebElement getElementByXPath(String xPath) {
        return driver.findElement(new By.ByXPath(xPath));
    }

    public void setCodeText(String text) {
        new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                .until(ExpectedConditions.visibilityOfElementLocated
                        (new By.ByXPath("//textarea[@name='PostForm[text]']")));
        WebElement textArea = getElementByXPath("//textarea[@name='PostForm[text]']");
        textArea.sendKeys(text);
    }

    public void setPasteName(String name) {
        WebElement pastName = getElementByXPath("//input[@name='PostForm[name]']");
        pastName.sendKeys(name);
    }

    public void setExpiration(String expiration) {
        WebElement expirationTime = getElementByXPath("//div[@class='form-group field-postform-expiration']/descendant::span[@class='select2-selection__arrow']");
        expirationTime.click();
        WebElement time = getElementByXPath("//li[text()='" + expiration + "']");
        time.click();
    }

    public void setSyntaxHighlighting(String syntaxHighlighting) {
        WebElement expirationTime = getElementByXPath("//div[@class='form-group field-postform-format']/descendant::span[@class='select2-selection__arrow']");
        expirationTime.click();
        WebElement time = getElementByXPath("//li[text()='" + syntaxHighlighting + "']");
        time.click();
    }

    public void submit() {
        WebElement createNewPastButton = getElementByXPath("//button[text()='Create New Paste']");
        createNewPastButton.click();
    }
}
