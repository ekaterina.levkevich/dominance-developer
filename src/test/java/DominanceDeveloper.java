import org.junit.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class DominanceDeveloper {
    private static final String codeText = "git config --global user.name  \"New Sheriff in Town\"\n" +
                                           "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                                           "git push origin master --force";
    private static final String syntaxHighlight = "Bash";
    private static final String expirationTime = "10 Minutes";
    private static final String pasteName = "how to gain dominance among developers";

    @Test
    public void newPasteCreationAndCheck () {
        WebDriver driver = new ChromeDriver();
        MainPastebinPage mainPage = new MainPastebinPage(driver);
        mainPage.setCodeText(codeText);
        mainPage.setSyntaxHighlighting(syntaxHighlight);
        mainPage.setExpiration(expirationTime);
        mainPage.setPasteName(pasteName);
        mainPage.submit();

        NewPastePage newPastePage = new NewPastePage(driver);
        assertEquals(codeText, newPastePage.getCodeText());
        assertEquals(syntaxHighlight, newPastePage.getSyntaxHighlighting());
        assertEquals(pasteName, newPastePage.getTitle());

        driver.quit();
    }

}
