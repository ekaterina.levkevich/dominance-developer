import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.time.temporal.ChronoUnit;

public class NewPastePage {
    private WebDriver driver;
    public NewPastePage(WebDriver driver){
        this.driver = driver;
    }
    private WebElement getElementByXPath(String xPath) {
        return driver.findElement(new By.ByXPath(xPath));
    }

    public String getTitle() {
        return getElementByXPath("//h1").getText();
    }

    public String getCodeText() {
        new WebDriverWait(driver, Duration.of(10, ChronoUnit.SECONDS))
                .until(ExpectedConditions.visibilityOfElementLocated
                        (new By.ByXPath("//div[@class='right']/child::a[1]")));
        WebElement rawButton = getElementByXPath("//div[@class='right']/child::a[1]");
        rawButton.click();
        String text = getElementByXPath("//pre").getText();
        driver.navigate().back();
        return text;
    }

    public String getSyntaxHighlighting() {
        return getElementByXPath("//div[@class='left']/descendant::a[@class='btn -small h_800']").getText();
    }
}
